import React from 'react'
import { Auth, HeaderItem } from '../../utils/Items'
import { useNavigate} from 'react-router-dom'

const Index = () => {
    const navigate=useNavigate()
    const handleClick=(e)=>{
        console.log("data",e)
        navigate(e)

    }
  return (
    <div style={{display:"flex",justifyContent:"space-between"}}>
        <div>
            Logo
        </div>
        <div style={{display:"flex",gap:"20px"}}>
     {
        HeaderItem?.map((item)=>(
            <div key={item.link} onClick={()=>handleClick(item.link)}>
                {item.name}
            </div>
        ))
     }
    </div >
   <div style={{display:"flex",gap:"4px"}}>
    {
         
            Auth?.map((item)=>(
                <div key={item.link} style={{color:"white"}} >
                    {item.name}
                </div>
            ))
        
    }
   </div>
    
    </div>
  )
}

export default Index
