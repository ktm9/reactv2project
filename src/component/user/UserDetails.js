import { EyeOutlined } from "@ant-design/icons";
import { Rate } from "antd";
import Card from "antd/es/card/Card";
import React from "react";

const UserDetails = () => {
  const myValue = localStorage.getItem("userdetail");
  const data = JSON.parse(myValue);
  console.log("userdetail", myValue);
  return (
    <div style={{ display: "flex", justifyContent: "flex-start", gap: "4px" }}>
      <div>
        userdetail
        <Card
          hoverable
          style={{ width: 240 }}
          cover={<img alt="example" src={data?.image} />}
        ></Card>
      </div>
      <div>
        <div>{data.name}</div>
        <div>
          Rate:
          <Rate value={data?.rate} />
        </div>
        <div>
          
          <EyeOutlined/>view:{data?.view}
        </div>
      </div>
      <div></div>
    </div>
  );
};

export default UserDetails;
