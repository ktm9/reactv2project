import React from "react";
import { Card } from "antd";
import { json, useNavigate } from "react-router-dom";

const UserItems = ({ data, title }) => {
  console.log("data", data, title);
  const navigate=useNavigate()
  const handleClick=(item)=>(
    navigate(`/userdetail/:${item.id}`),
    localStorage.setItem('userdetail',JSON.stringify(item))
    
  )

  return (
    <div>
      <div>{title}</div>
      <div
        style={{ display: "flex", justifyContent: "flex-start", gap: "4px" }}
      >
        {data?.map((item) => (
          <div key={item.id} onClick={()=>handleClick(item)}>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={<img alt="example" src={item.image} />}
            >
              <div>price: {item.price}</div>
              <div>brand: {item.brand}</div>
              

            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserItems;
