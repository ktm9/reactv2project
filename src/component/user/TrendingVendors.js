import React from 'react'
import {  TrendingVendorData } from '../../utils/Items'
import UserItems from './UserItems'

const TrendingVendors = () => {
  return (
    <div>
      <UserItems data={TrendingVendorData} title="Trending vendor"/>
    </div>
  )
}

export default TrendingVendors
