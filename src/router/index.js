import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import AdminLayout from "../layout/AdminLayout";
import AdminDashBoard from "../component/admin/DashBoard";
import UserLayout from "../layout/UserLayout";
import UserDashBoard from "../component/user/DashBoard";
import AboutUS from "../component/aboutUs/AboutUS";
import PageNotFound from "../component/PageNotFound";
import UserDetails from "../component/user/UserDetails";


export const Routers = createBrowserRouter(
    createRoutesFromElements(
      <Route>
        <Route path="/" element={<UserLayout/>}>
            <Route index element={<UserDashBoard/>}/>
            <Route path="aboutus" element={<AboutUS/>}></Route>
            <Route path="userdetail/:id" element={<UserDetails/>}></Route>
            
          
        </Route>
        <Route path="/admin" element={<AdminLayout/>}>
            <Route index element={<AdminDashBoard/>}/>
        </Route>
        <Route path="*" element={<PageNotFound/>}/>
        
        
       
      </Route>
    )
  );
  