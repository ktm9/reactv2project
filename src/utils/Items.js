export const HeaderItem = [
  {
    name: "About US",
    link: "/aboutus",
  },
  {
    name: "Contact US",
    link: "/contactus",
  },
  {
    name: "Hot products",
    link: "/hotproducts",
  },
  {
    name: "Latest products",
    link: "/latestproducts",
  },
];
export const Auth = [
  {
    name: "Log in",
    link: "/login",
  },
  {
    name: "Sign up",
    link: "/signup",
  },
];

export const HotProductData = [
  {
    name: "T-shirt",
    price: "100",
    brand: "Gucci",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2020/10/Chelsea-Third-Jersey-20232024-.jpg",
    id: 1,
    id: 2,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:45,
    view:145
  },
  {
    name: "bag",
    price: "200",
    brand: "Gucci",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2020/10/Chelsea-Third-Jersey-20232024-.jpg",
    id: 2,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:45,
    view:129  
  },
  {
    name: "jacket",
    price: "300",
    brand: "zara",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2020/10/Chelsea-Third-Jersey-20232024-.jpg",
    id: 3,
    rate: 5,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:46,
    view:129  
  },
  {
    name: "shoes",
    price: "400",
    brand: "jordan",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2020/10/Chelsea-Third-Jersey-20232024-.jpg",
    id: 4,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:5,
    view:130 
  },
];
export const TrendingVendorData = [
  {
    name: "T-shirt",
    price: "500",
    brand: "Gucci",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Soccer-Jersey-20232024-6.jpg",
    id: 1,
  },
  {
    name: "bag",
    price: "600",
    brand: "Gucci",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Soccer-Jersey-20232024-6.jpg",
    id: 2,
  },
  {
    name: "jacket",
    price: "700",
    brand: "zara",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Soccer-Jersey-20232024-6.jpg",
    id: 3,
  },
  {
    name: "shoes",
    price: "800",
    brand: "jordan",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Soccer-Jersey-20232024-6.jpg",
    id: 4,
  },
];
